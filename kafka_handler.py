#pip2 install kafka-python
from kafka import KafkaConsumer,TopicPartition

import json
from tensorflow_serving.apis import predict_pb2
from tensorflow_serving.apis import prediction_service_pb2
from grpc.beta import implementations
import numpy
import numpy as np 
import tensorflow as tf
import time


LOCALHOST ="127.0.0.1"
def _create_rpc_callback():

	def _callback(result_future):
		"""Callback function.

		Calculates the statistics for the prediction result.

		Args:
			result_future: Result future of the RPC.
		"""
		exception = result_future.exception()
		if exception:
			print(exception)
		else:
			scores = numpy.array(result_future.result().outputs['scores'].float_val)
			ids = numpy.array(result_future.result().outputs['scores'].int64_val)
			#do something...
			
	return _callback

def createRequest(stub,item_list):
	request = predict_pb2.PredictRequest()
	request.model_spec.name = 'recommender'
	request.model_spec.signature_name = 'recommendation'
	request.inputs['items'].CopyFrom(
		tf.contrib.util.make_tensor_proto(np.array(item_list,dtype =np.int64)))
	result_future = stub.Predict.future(request,10.0)
	result_future.add_done_callback(_create_rpc_callback())

import time

def __main__():
	
	host = LOCALHOST
	port = 4124
	
	# create kafka consumer
	consumer = KafkaConsumer(bootstrap_servers='localhost:9092')
	consumer.subscribe(['rec'])
	
	# create tensorflow-serving connector
	channel = implementations.insecure_channel(host,int(port))
	stub = prediction_service_pb2.beta_create_PredictionService_stub(channel)

	for msg in consumer:
		print('new message comming around.')
		data = msg.value.decode()
		item_list = json.loads(data.decode())
		createRequest(stub,item_list)

__main__()



